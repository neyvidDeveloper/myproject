<?php


// Route For FrontEnd
Route::group(['namespace' => 'FrontEnd\\'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    //Login Routes
    Route::get('/login', 'AuthController@loginForm')->name('loginForm')->middleware('guest');
    Route::post('/login', 'AuthController@login')->name('login');
    //LogOut Routes
    Route::get('/logout', 'AuthController@logout')->name('logout');
    //Register Routes
    Route::get('/register', 'AuthController@registerForm')->name('registerForm');
    Route::post('/register', 'AuthController@register')->name('register');
    //Reset Password
    Route::get('/reset', 'AuthController@resetPawsswordForm')->name('passResetForm');
    Route::post('/reset', 'AuthController@resetPassword')->name('resetPassword');
    Route::get('/reset/{token}', 'AuthController@resetPassWithEmail')->name('resetPassWithEmail');
    Route::post('/reset/{token}', 'AuthController@setNewPassword')->name('setNewPassword');
//Route Of Courses
    Route::get('/courses', 'CourseController@index')->name('course.index');
    Route::get('/courses/single', 'CourseController@showSingleCourse')->name('courses.single');
    Route::get('/basket/add/{course_id}', 'BasketController@add')->name('basket.add');
    Route::get('/basket/show', 'BasketController@index')->name('basket.show');
    Route::post('/basket/update', 'BasketController@update')->name('basket.update');
    Route::get('basket/checkout','BasketController@checkout')->name('basket.checkout');
});

// Route For Admin or Agent
Route::group(['prefix' => 'admin/', 'namespace' => 'Admin\\', 'middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('roles', 'RoleController@index')->name('admin.roles');
    Route::get('users', 'UserController@index')->name('admin.users');
    Route::get('course', 'CourseController@index')->name('admin.course');
    Route::get('course/verify', 'CourseController@verifyCourse')->name('admin.course.verify');
    Route::get('course/delete', 'CourseController@delete')->name('admin.course.delete');
    Route::get('course/edit', 'CourseController@edit')->name('admin.course.edit');
    Route::post('course/edit', 'CourseController@update')->name('admin.course.update');
    Route::get('course/create', 'CourseController@create')->name('admin.course.create');
    Route::post('course/create', 'CourseController@store')->name('admin.course.store');
    Route::get('users/requests', 'RequestController@index')->name('admin.user.request');
    Route::get('users/requests/confirm/{id?}', 'RequestController@requestConfirm')->name('admin.user.drequest.confirm');
    Route::get('group', 'GroupController@index')->name('admin.group');
    Route::get('group/delete', 'GroupController@delete')->name('admin.group.delete');
    Route::get('group/edit', 'GroupController@edit')->name('admin.group.edit');
    Route::post('group/edit', 'GroupController@update')->name('admin.group.update');
    Route::get('group/create', 'GroupController@create')->name('admin.group.create');
    Route::post('group/create', 'GroupController@store')->name('admin.group.store');

});


//Route For Students
Route::group(['prefix' => 'student', 'namespace' => 'Student\\'], function () {
    Route::get('/', 'StudentController@index')->name('student');
    Route::get('/info', 'StudentController@studentInfo')->name('student.info');
    Route::post('/info/edit/{st_id?}', 'StudentController@studentInfoEdit')->name('student.edit');
    Route::post('/info/update/{st_id?}', 'StudentController@studentInfoUpdate')->name('student.edit');
    Route::post('/request', 'StudentController@sendRequest')->name('student.request');
    Route::get('/courses', 'TeacherController@index')->name('teacher.courses');
});
//Route For teacher
Route::group(['prefix' => 'teacher', 'namespace' => 'Teacher\\'], function () {


    Route::get('/courses', 'TeacherController@index')->name('teacher.courses');
    Route::get('/courses/create', 'TeacherController@create')->name('teacher.courses.create');
    Route::post('/courses/create', 'TeacherController@store')->name('teacher.courses.store');
});




