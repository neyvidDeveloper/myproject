<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:8',
            'password_repeat' => 'required|min:8'
        ];
    }

    public function messages()
    {
        return [
            'password.required'=>'رمز را وارد نمایید',
            'password_repeat.required'=>'لطفا تکرار رمز را وارد نمایید',
            'password.min'=>'رمز عبور حداقل باید 8 کاراکتر باشد',
            'password_repeat.min'=>'رمز عبور حداقل باید 8 کاراکتر باشد',
        ];
    }
}
