<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'لطفا نام خود را وارد نمایید',
            'lastname.required' => 'لطفا نام خانوادگی خود را وارد نمایید',
            'email.required' => 'لطفا ایمیل خود را وارد نمایید',
            'email.email' => 'لطفا ایمیل خود را بصورت صحیح وارد نمایید',
            'password.required' => 'لطفا رمزعبور  را وارد نمایید',
            'password.min' => 'رمز عبور حداقل باید 8 کاراکتر باشد',
        ];
    }
}
