<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Requests\UserRegister;
use App\Http\Requests\UserResetPassword;
use App\Mail\ResetPassword;
use App\Models\User;
use App\Repository\User\PasswordResetRepository;
use App\Repository\User\UserRepository;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public $userRepository;
    public $passwordResetRepository;

    public function __construct()
    {
        $this->passwordResetRepository = new PasswordResetRepository();
        $this->userRepository = new UserRepository();
    }

    //show LoginForm
    public function loginForm()
    {
        return view('frontend.user.loginform');
    }

    //show RegisterForm
    public function registerForm()
    {
        $title = 'خوش آمدید';
        return view('frontend.user.registerform', compact('title'));
    }

    //Register New User To System
    public function register(UserRegister $request)
    {

        $data = $request->all();
        $userCreated = UserService::create($data);
        $userCreated->assignRole('student');
        if ($userCreated) {
            return redirect()->route('loginForm')->with('success', 'ثبت نام شما با موفقیت انجام شد,میتوانید وارد سایت شوید');
        }
    }

    //Login User Into System
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect('/');
        }
        return redirect()->back()->with('error', 'ایمیل و یا رمز عبور رابصورت صحیح وارد نمایید');

    }

    //LogOUt User Of System
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function resetPawsswordForm()
    {
        return view('frontend.user.resetpassform');
    }

    public function resetPassword(Request $request)
    {
        $user = $this->userRepository->all()->where('email', $request->input('email'))->first();
        if ($user == null) {
            return 403;
        }
        $token = str_random(60);
        $resetPasswordCreated = $this->passwordResetRepository->create([
            'email' => $request->input('email'),
            'token' => $token,
        ]);
        if ($resetPasswordCreated) {
            try {
                Mail::to($user)->send(new ResetPassword($user, $token));
                return redirect()->route('loginForm')->with('success', 'لینک بازیابی رمز عبور برای ایمیل شما ارسال گردید لطفا ایمیل خود را کنترل نمایید');
            } catch (\Exception $exception) {
                abort(404);
            }
        }


    }

    public function resetPassWithEmail()
    {
        return view('frontend.user.changePasswordForm');
    }

    public function setNewPassword(UserResetPassword $request)
    {

        $userEmail = $this->passwordResetRepository->all()->where('token', $request->token)->first()->email;
        $user = $this->userRepository->all()->where('email', $userEmail)->first();
        $user->password = $request->input('password');
        $saveUserPass = $user->save();
        if ($saveUserPass) {
            return redirect()->route('loginForm')->with('success', 'رمز عبور شما با موفقیت تغییر کرد و شما می توانید وارد سایت شوید');
        }

    }
}
