<?php

namespace App\Http\Controllers\FrontEnd;


use App\Http\Controllers\Controller;
use App\Services\Basket\BasketService;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;


class BasketController extends Controller
{
    public function index()
    {
        $basketItems = BasketService::getBasket();
        return view('frontend.basket.index', compact('basketItems'));

    }

    public function add(int $course_id)
    {
        BasketService::add($course_id);
        return redirect()->route('basket.show');
    }

    public function update(Request $request)
    {
        $courseId = $request->course_id;
        $action = $request->action;
        $result = BasketService::update($courseId, $action);
        if ($result['success']) {
            $result['subTotal'] = BasketService::subTotal($courseId);
            $result['total'] = BasketService::total();

        }
        return $result;

    }

    public function checkout()
    {
        $items = session()->get('basket.item');
        $orderData = [
            'user_id' => 1,
            'amount' => BasketService::total(),
            'discount' => 0,
            'payable' => BasketService::total(),
            'paymentMethod' => 1,
            'status' => 0,
            'order_item' => []
        ];
        foreach ($items as $course_id => $course) {
            $orderData['order_item'][] = [
                'course_id' => $course_id,
                'amount' => $course['course_item']->price,
                'count'=>$course['count'],
            ];
        }
        $orderService=new OrderService();
        $orderService->create($orderData);


    }
}