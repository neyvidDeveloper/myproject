<?php

namespace App\Http\Controllers\FrontEnd;

use App\Repository\Course\CoureRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public $CourseRepository;

    public function __construct()
    {
        $this->CourseRepository = new CoureRepository();

    }

    public function index()
    {
        $courses = $this->CourseRepository->all();
        return view('frontend.course.index', compact('courses'));
    }

    public function showSingleCourse(Request $request)
    {
        $courses = $this->CourseRepository->find($request->id);
        return view('frontend.course.single',compact('courses'));

    }
}
