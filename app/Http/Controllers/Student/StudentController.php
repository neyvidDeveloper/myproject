<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\StudentInfoUpdate;
use App\Models\User;
use App\Repository\User\UserRepository;
use App\Repository\User\UserRequestRepository;
use App\Services\UploaderFile\UploadFile;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Config;

class StudentController extends Controller
{
    public $userRepository;
    public $requestUserRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->requestUserRepository = new UserRequestRepository();
    }

    public function index()
    {
        return view('student.dashboard.index');
    }

    public function studentInfo()
    {
        $user = Auth::user();
        return view('student.studentinfo.index', compact('user'));
    }

    public function studentInfoEdit(Request $request)
    {
        $studentId = $request->input('st_id');
//        if (Auth::id() != $studentId) {   if  one user send id of oter user
//            return false;
//        }
        $userInfo = $this->userRepository->find($studentId);
        return $userInfo;


    }

    public function studentInfoUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
        ], [
            'name.required' => 'نام لازم هست پر شود',
            'name.min' => 'نام لازم هست بیشتر از 4 حرف باشد',
        ]);


        if ($validator->fails()) {
            return ['error' => $validator->errors()];
        };
        if ($request->input('studentid') != Auth::id()) {
            return [
                'error' => ['خطایی اتفاق افتاده است لطفا مجددا سعی نمایید'],  //true result
            ];
        }
        $studentUpdated = $this->userRepository->update($request->input('studentid'), [
            'name' => $request->input('name'),
            'lastname' => $request->input('lastname'),
            'phone' => $request->input('phone'),
            'mobile' => $request->input('mobile'),
            'cv' => $request->input('cv'),
        ]);
        if ($request->hasFile('studentPhoto')) {
            $file = $request->file('studentPhoto');
            $fileExtension = $file->getClientOriginalExtension();
            $newFileName = str_random(10) . '.' . $fileExtension;
            $file->move(config('upload.path'), $newFileName);
            $this->userRepository->update($request->input('studentid'), [
                'photo' => $newFileName
            ]);
        }
        if ($studentUpdated) {
            return [
                'success' => $studentUpdated,  //true result
                'studentInfo' => Auth::user()->fresh(),
            ];
        }


    }

    public function sendRequest(Request $request)
    {
        $currentStudentId = Auth::id();

        if ($currentStudentId == $request->input('student_id')) {
            $this->requestUserRepository->create([
                'user_id'=>$request->input('student_id'),
                'url'=>$request->input('requestLink'),
                'subject'=>$request->input('requestTitle'),
                'description'=>$request->input('requestDesc'),

            ]);
        }
    }


}
