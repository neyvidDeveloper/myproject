<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Course\Days;
use App\Services\Course\CourseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{

    public function index(Request $request)
    {

        $title='لیست دوره های شما';
        $allCourseOfTeacher=Auth::user()->courses;
        return view('teacher.index',compact('title','allCourseOfTeacher'));
    }
    public function create()
    {
        $title='ایجاد دوره جدید';
        $days=Days::allDays();
        return view('teacher.create',compact('title','days'));
    }
    public function store(Request $request)
    {
        CourseService::StoreCourse([
            'user_id' => Auth::id(),
            'group_id' => 1,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'startOfPresent' => $request->input('startOfPresent'),
            'endOfPresent' => $request->input('endOfPresent'),
            'timeOfCourse' => $request->input('timeOfCourse'),
            'daysOfPresent' => serialize($request->input('days')),
            'capacity' => $request->input('capacity'),
            'type' => 1,
            'price' => $request->input('price'),
        ]);
    }



}
