<?php

namespace App\Http\Controllers\Admin;

use App\Repository\Role\RoleRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct()
    {
        $this->roleRepository = new RoleRepository();
    }

    public function index()
    {
        $title='لیست نقش های کاربران';
        $allRoles = $this->roleRepository->all();
        return view('admin.role.index',compact('allRoles','title'));
    }

}
