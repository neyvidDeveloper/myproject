<?php

namespace App\Http\Controllers\Admin;

use App\Mail\VerifyUserRequest;
use App\Repository\User\UserRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    protected $useRequestRepository;

    public function __construct()
    {
        $this->useRequestRepository = new UserRequestRepository();
    }

    public function index()
    {
        $title = 'لیست درخواست های کاربران';
        $allRequest = $this->useRequestRepository->all();
        return view('admin.request.index', compact('title', 'allRequest'));
    }

    public function requestConfirm(Request $request)
    {
        $requestId = $request->id;
        $request = $this->useRequestRepository->find($requestId);
        $request->status = 2;
        if ($request->save()) {
            $userOfRequest = $request->user;
            $userOfRequest->syncRoles('teacher');
            Mail::to($userOfRequest)->send(new VerifyUserRequest($userOfRequest));
        }
    }
}
