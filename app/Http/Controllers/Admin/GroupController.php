<?php

namespace App\Http\Controllers\Admin;

use App\Repository\Group\GroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public $groupRepository;

    public function __construct()
    {
        $this->groupRepository = new GroupRepository();
    }

    public function index()
    {

        $title = 'لیست گروه بندی ها';
        $allGroup = $this->groupRepository->all();
        return view('admin.group.index', compact('title', 'allGroup'));
    }

    public function create()
    {
        $title = 'ایجاد گروه بندی جدید';
        $allGroup = $this->groupRepository->all()->groupBy('parent_id');
        return view('admin.group.create', compact($title), compact('title', 'allGroup'));

    }

    public function store(Request $request)
    {
        $this->groupRepository->create([
            'title' => $request->input('title'),
            'parent_id' => $request->input('groupParent'),
        ]);
    }

    public function delete(Request $request)
    {
        $current_id = $request->id;
        $this->groupRepository->delete($current_id);
    }

    public function edit(Request $request)
    {
        $title = 'ویرایش گروه';
        $current_id = $request->id;
        $current_group = $this->groupRepository->find($current_id);
        $allGroup = $this->groupRepository->all();
        return view('admin.group.edit', compact('title', 'current_group', 'allGroup'));
    }

    public function update(Request $request)
    {
        $current_id = $request->id;
        $this->groupRepository->update($current_id, [
            'title' => $request->input('title'),
            'parent_id' => $request->input('groupParent'),
        ]);
    }
}
