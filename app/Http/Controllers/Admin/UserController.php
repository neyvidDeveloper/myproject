<?php

namespace App\Http\Controllers\Admin;

use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function index()
    {
        $title='لیست کاربران سایت';
        $allUsers=$this->userRepository->all();
        return view('admin.user.index',compact('title','allUsers'));
    }
}
