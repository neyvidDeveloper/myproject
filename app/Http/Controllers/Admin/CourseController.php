<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course\Days;
use App\Repository\Course\CoureRepository;
use App\Services\Course\CourseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    protected $courseRepository;

    public function __construct()
    {
        $this->courseRepository = new CoureRepository;
    }

    public function index()
    {
        $title = 'لیست دوره های';
        $courses = $this->courseRepository->all();
        return view('admin.course.index', compact('title', 'courses'));
    }

    public function create()
    {
        $title = 'ایجاد دوره جدید';
        $days = Days::allDays();
        return view('admin.course.create', compact('title', 'days'));

    }

    public function store(Request $request)
    {
        CourseService::StoreCourse([
            'user_id' => Auth::id(),
            'group_id' => 1,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'startOfPresent' => $request->input('startOfPresent'),
            'endOfPresent' => $request->input('endOfPresent'),
            'timeOfCourse' => $request->input('timeOfCourse'),
            'daysOfPresent' => serialize($request->input('days')),
            'capacity' => $request->input('capacity'),
            'type' => 1,
            'price' => $request->input('price'),
        ]);
//
//        $this->courseRepository->create([
//            'user_id' => Auth::id(),
//            'group_id' => 1,
//            'title' => $request->input('title'),
//            'description' => $request->input('description'),
//            'startOfPresent' => $request->input('startOfPresent'),
//            'endOfPresent' => $request->input('endOfPresent'),
//            'timeOfCourse' => $request->input('timeOfCourse'),
//            'daysOfPresent' => serialize($request->input('days')),
//            'capacity' => $request->input('capacity'),
//            'type' => 1,
//            'status' => $request->input('status'),
//            'price' => $request->input('price'),
//        ]);
//        dd($request->all());
    }

    public function delete(Request $request)
    {
        $itemDeleted = $this->courseRepository->delete($request->id);
        if ($itemDeleted) {
            return back()->with('success', 'دوره مورد نظر شما با موفقیت حذف گردید');
        }
    }

    public function edit(Request $request)
    {
        $title = 'ویرایش دوره';
        $currentCourseId = $request->input('id');
        $days = Days::allDays();
        $currentCourse = $this->courseRepository->find($currentCourseId);
        return view('admin.course.edit', compact('title', 'currentCourse', 'days'));
    }

    public function update(Request $request)
    {
        $current_id = $request->id;

        $this->courseRepository->update($current_id, [
            'user_id' => Auth::id(),
            'group_id' => 1,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'startOfPresent' => $request->input('startOfPresent'),
            'endOfPresent' => $request->input('endOfPresent'),
            'timeOfCourse' => $request->input('timeOfCourse'),
            'daysOfPresent' => serialize($request->input('days')),
            'capacity' => $request->input('capacity'),
            'type' => 1,
            'status' => $request->input('status'),
            'price' => $request->input('price'),
        ]);

    }

    public function verifyCourse(Request $request)
    {

        $courseId = $request->id;
        $courserItem = $this->courseRepository->find($courseId);
        if ($courserItem->status == 0) {
            $courserItem->status = 2;
            $courserItem->save();
        }
        return back();


    }
}
