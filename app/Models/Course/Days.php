<?php


namespace App\Models\Course;


class Days
{
    const  Saturday = 0;
    const  Sunday = 1;
    const  Monday = 2;
    const  Tuesday = 3;
    const  Wednesday = 4;
    const  Thursday = 5;
    const  Friday = 6;

    public static function allDays()
    {
        return [
            self::Saturday => 'شنبه',
            self::Sunday => 'یکشنبه',
            self::Monday => 'دوشنبه',
            self::Tuesday => 'سه شنبه',
            self::Wednesday => 'چهارشنبه',
            self::Thursday => 'پنجشنبه',
            self::Friday => 'جمععه',
        ];
    }

    public static function getDay(int $numberOfDay)
    {
        return self::allDays()[$numberOfDay];
    }



}