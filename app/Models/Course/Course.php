<?php

namespace App\Models\Course;

use App\Models\User;
use App\presenters\Contract\Presentable;
use App\presenters\CoursePresenter;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use Presentable;
    protected $dates=['startOfPresent','endOfPresent'];
    protected $guarded=['id'];
    public $presenter=CoursePresenter::class;
    public function teacher()
    {
       return $this->belongsTo(User::class,'user_id','id');
    }

    public function course_users()
    {
        return $this->belongsToMany(User::class, 'user_couorses', 'course_id', 'user_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
