<?php

namespace App\Models\Course;

use App\presenters\Contract\Presentable;
use App\presenters\GroupPresenter;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use Presentable;
    protected $table = 'groupes';
    protected $guarded = ['id'];
    public $presenter=GroupPresenter::class;

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function parent()
    {
        return $this->belongsTo(Group::class, 'parent_id','id');
    }

    public function childs()
    {
        return $this->hasMany(Group::class, 'parent_id','id');
    }
}
