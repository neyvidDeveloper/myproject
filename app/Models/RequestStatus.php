<?php


namespace App\Models;


class RequestStatus
{
    const NOTCONFIRM=0;
    const PENDING=1;
    const CONFIRM=2;

    public static function getRequestStatuses()
    {
        return [
            self::NOTCONFIRM=>'تایید نشده',
            self::PENDING=>'درحال بررسی',
            self::CONFIRM=>'تایید شده',
        ];
    }
    public static function RequestHtmlClass()
    {
        return [
            self::NOTCONFIRM=>'primary',
            self::PENDING=>'warning',
            self::CONFIRM=>'success',
        ];
    }

    public static function getRequestStatus(int $requestStatus)
    {
        return self::getRequestStatuses()[$requestStatus];
    }
    public static function getRequestHtmlClass(int $requestStatus)
    {
        return self::RequestHtmlClass()[$requestStatus];
    }

    

}