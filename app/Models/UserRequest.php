<?php

namespace App\Models;

use App\presenters\Contract\Presentable;
use App\presenters\UserRequestPresenter;
use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    use Presentable;
    protected $guarded = ['id'];
    public $table = 'user_requests';
    public $presenter =UserRequestPresenter::class;
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
