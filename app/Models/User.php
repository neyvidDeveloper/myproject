<?php

namespace App\Models;

use App\Models\Course\Course;
use App\Models\Order\Order;
use App\presenters\Contract\Presentable;
use App\presenters\StudentPresenter;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, CanResetPassword, Presentable;

//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $presenter = StudentPresenter::class;

    public function courses()
    {
       return $this->hasMany(Course::class,'user_id','id');
    }

    public function user_courses()
    {
        return $this->belongsToMany(Course::class, 'user_courses', 'user_id', 'course_id');
    }

    public function orders()
    {
        return  $this->hasMany(Order::class);
    }

    public function request()
    {
        return $this->hasOne(UserRequest::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

}
