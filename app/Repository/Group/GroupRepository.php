<?php


namespace App\Repository\Group;


use App\Models\Course\Group;
use App\Repository\Contract\BaseRepository;

class GroupRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=Group::class;
    }
}