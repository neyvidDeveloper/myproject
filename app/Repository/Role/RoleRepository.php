<?php


namespace App\Repository\Role;


use App\Repository\Contract\BaseRepository;
use Spatie\Permission\Models\Role;

class RoleRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = Role::class;
    }


}