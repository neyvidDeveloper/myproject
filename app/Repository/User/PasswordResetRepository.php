<?php


namespace App\Repository\User;


use App\Models\PasswordReset;
use App\Repository\Contract\BaseRepository;

class PasswordResetRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=PasswordReset::class;
    }
}