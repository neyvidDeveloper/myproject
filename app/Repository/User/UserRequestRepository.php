<?php


namespace App\Repository\User;


use App\Models\UserRequest;
use App\Repository\Contract\BaseRepository;

class UserRequestRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=UserRequest::class;
    }
}