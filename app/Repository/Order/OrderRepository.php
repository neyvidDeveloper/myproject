<?php


namespace App\Repository\Order;


use App\Models\Order\Order;
use App\Repository\Contract\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=Order::class;
    }

}