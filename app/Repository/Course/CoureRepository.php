<?php


namespace App\Repository\Course;


use App\Models\Course\Course;
use App\Repository\Contract\BaseRepository;

class CoureRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=Course::class;
    }
}