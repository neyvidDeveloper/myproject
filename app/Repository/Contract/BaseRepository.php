<?php


namespace App\Repository\Contract;


abstract class BaseRepository
{
    protected $model;

    public function find(int $id)
    {
        return $this->model::find($id);
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function update(int $id, array $data)
    {
        $item = $this->find($id);
        return $item->update($data);
    }

    public function delete($id)
    {
        $item = $this->find($id);
        return $item->delete();
    }

    public function findBy(array $criteria, $relation = [], $single = true)
    {
        $query = $this->model::query();
        if (!is_null($relation)) {
            $query = $this->model->with($relation);
        }
        foreach ($criteria as $key => $index) {


            $query->where($key, $index);
        }
        if ($single) {
            return $query->first();
        }
        return $query->get();
    }
}