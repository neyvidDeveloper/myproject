<?php


namespace App\Services\Order;


use App\Repository\Order\OrderRepository;

class OrderService
{
    public $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();

    }




    public function create(array $data)
    {

        $newOrder = $this->orderRepository->create([
            'user_id' => $data['user_id'],
            'amount' => $data['amount'],
            'discount' => $data['discount'],
            'payable' => $data['payable'],
            'paymentMethod' => $data['paymentMethod'],
            'status' => $data['status'],
        ]);
        $orderItems = [];
        foreach ($data['order_item'] as $order) {
            $orderItems[] = [
                'order_id' => $newOrder->id,
                'course_id' => $order['course_id'],
                'amount' => $order['amount'],
                'count' => $order['count'],
            ];

        }
        $newOrder->order_items()->createMany($orderItems);
    }

}