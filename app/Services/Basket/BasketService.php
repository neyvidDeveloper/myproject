<?php


namespace App\Services\Basket;


use App\Repository\Course\CoureRepository;

class BasketService
{
    public static function add(int $course_id)
    {
        $courseRepository = new CoureRepository();
        $courseItem = $courseRepository->find($course_id);
        if ($courseItem) {
            $items = self::getBasket();
            if (is_null($items)) {
                $items = [];
            }
            if (isset($items[$course_id])) {
                $items[$course_id]['count'] += 1;
            } else {
                $items[$course_id] = [
                    'count' => 1,
                    'course_item' => $courseItem
                ];
            }
            session(['basket.item' => $items]);
        }

    }

    public static function getBasket()
    {
        return session('basket.item');
    }

    public static function update($courseId, $action)
    {
        $items = self::getBasket();
        if (isset($items[$courseId])) {
            $course = $items[$courseId]['course_item'];
            $course = $course->fresh();
            $current_count = $items[$courseId]['count'];
            if ($action == 'plus') {
                if ($current_count < $course->capacity) {
                    $items[$courseId]['count'] += 1;
                    $items[$courseId]['course_item'] = $course;
                    session(['basket.item' => $items]);
                } else {
                    return
                        [
                            'success' => false,
                            'message' => 'امکان انتخاب بیشتر از ظرفیت نمی باشد'
                        ];
                };

            };
            if ($action == 'minus') {
                if ($current_count > 1) {
                    $items[$courseId]['count'] -= 1;
                    $items[$courseId]['course_item'] = $course;
                    session(['basket.item' => $items]);
                } else {
                    return
                        [
                            'success' => false,
                            'message' => 'حداقل یک مورد باید موجود باشد. درصورت عدم نیاز مورد مربوطه را حذف نمایید'
                        ];

                };
            };
            return ['success' => true];
        } else {
            return [
                'success' => false,
                'message' => 'این محصول موجود نمی باشد'
            ];
        }
    }

    public static function subTotal(int $course_id)
    {
        $items = self::getBasket();
        if (isset($items[$course_id])) {
            return $items[$course_id]['course_item']->price * $items[$course_id]['count'];
        }
        return 0;

    }

    public static function total()
    {
        $total=0;
        $items = self::getBasket();
        foreach ($items as $product_id => $item) {
            $total += $item['course_item']->price * $item['count'];

        };
        return $total;

    }
}