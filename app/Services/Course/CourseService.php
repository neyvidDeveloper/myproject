<?php


namespace App\Services\Course;


use App\Repository\Course\CoureRepository;

class CourseService
{

    public static function StoreCourse(array $data)
    {
        $courseRepository = new CoureRepository();
        $courseCreated=$courseRepository->create([
            'user_id' =>$data['user_id'],
            'group_id' => $data['group_id'],
            'title' => $data['title'],
            'description' => $data['description'],
            'startOfPresent' => $data['startOfPresent'],
            'endOfPresent' => $data['endOfPresent'],
            'timeOfCourse' => $data['timeOfCourse'],
            'daysOfPresent' =>$data['daysOfPresent'],
            'capacity' => $data['capacity'],
            'type' =>  $data['type'],
            'status' => 0,
            'price' => $data['price'],
        ]);
        return $courseCreated;

    }
}