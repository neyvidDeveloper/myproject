<?php


namespace App\Services\User;


use App\Models\User;
use App\Repository\User\UserRepository;

class UserService
{

    public static function create(array $data)
    {
        $userRepository = new UserRepository();
        $userCreated = $userRepository->create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => $data['password'],
        ]);
        if($userCreated  && $userCreated instanceof User){
        //event handler
        }
        return $userCreated;
    }
}