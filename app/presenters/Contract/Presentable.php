<?php


namespace App\presenters\Contract;


trait Presentable
{
    protected $presenterInstance;

    public function present()

    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new \Exception('Presenter Not Found!');
        }
        $this->presenterInstance = new $this->presenter($this);
        return $this->presenterInstance;


    }

}