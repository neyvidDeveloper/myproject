<?php


namespace App\presenters;


use App\presenters\Contract\Presenter;

class UserRequestPresenter extends Presenter
{
    public function show_status()
    {
        $currentStatus=$this->entity->status;
        if($currentStatus==1){
            return  '<span class="label label-warning">درحال بررسی</span>';
        } if($currentStatus==2){
            return  '<span class="label label-success">تایید شده</span>';
        }

    }
}