<?php


namespace App\presenters;


use App\Models\Course\Days;
use App\Models\RequestStatus;
use App\presenters\Contract\Presenter;
use Hekmatinasser\Verta\Verta;

class CoursePresenter extends Presenter
{
    public function show_jalali_date()
    {
        $startOfPresent = $this->entity->startOfPresent->toDateString();
        $endOfPresent = $this->entity->endOfPresent->toDateString();
        $startOfPresent_jalali = new Verta($startOfPresent);
        $endOfPresentt_jalali = new Verta($endOfPresent);
        return ['startOfPresent_jalali' => $startOfPresent_jalali->format('%d %B، %Y'), 'endOfPresentt_jalali' => $endOfPresentt_jalali->format('%d %B، %Y')];
    }

    public function convert_to_timeStamp()
    {
        $startOfPresent = $this->entity->startOfPresent->getTimestamp();
        $endOfPresent = $this->entity->endOfPresent->getTimestamp();
        return ['startOfPresent' => $startOfPresent, 'endOfPresent' => $endOfPresent];
    }

    public function showDaysOfPresent()
    {
        $days = unserialize($this->entity->daysOfPresent);
        $dayPresent = '';
        foreach ($days as $day) {
            $dayPresent .= Days::getDay($day) . ',';
        }
        return $dayPresent;
    }

    public function show_nice_price()
    {
        $price = $this->entity->price;
        return number_format($price, 0) . '  تومان ';
    }

    public function show_capacity()
    {
        $capacity = $this->entity->capacity;
        return $capacity . '  نفر ';
    }

    public function show_alltime_of_course()
    {
        $timeOfCourse = $this->entity->timeOfCourse;
        return $timeOfCourse . '  ساعت ';
    }

    public function show_status_with_link()
    {
        $currentStatus = $this->entity->status;
        $currentId = $this->entity->id;
        return "<a href=" . route('admin.course.verify') . '/?id=' . $currentId . "><button  type='button' class='btn btn-" . RequestStatus::getRequestHtmlClass($currentStatus) . "' >" . RequestStatus::getRequestStatus($currentStatus) . "</button></a>";
    }

    public function show_status()
    {
        $currentStatus = $this->entity->status;
        return "<span class='label label-" . RequestStatus::getRequestHtmlClass($currentStatus) . "' > " . RequestStatus::getRequestStatus($currentStatus) . "</span > ";
    }
}