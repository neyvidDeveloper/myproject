<?php


namespace App\presenters;


use App\presenters\Contract\Presenter;

class StudentPresenter extends Presenter
{
    public function show_name_whit_lastname()
    {
        $name = $this->entity->name;
        $lastname = $this->entity->lastname;
        return $name . ' ' . $lastname;
    }
}