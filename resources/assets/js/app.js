/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));


const app = new Vue({
    el: '#app'
});
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.studentEdit', function (e) {
        e.preventDefault();
        $('.showError').css('display', 'none');
        $('.showSucess').css('display', 'none');
        var __this = $(this);
        var st_id = __this.data('stid');
        var name = $('#name');
        var email = $('#email');
        var lastname = $('#lastname');
        var phone = $('#phone');
        var mobile = $('#mobile');
        var photo = $('#photo');
        var cv = $('#cv');
        axios.post('info/edit', {
            st_id: st_id
        }).then(response => {
            name.val(response.data.name);
            lastname.val(response.data.lastname);
            email.val(response.data.email);
            phone.val(response.data.phone);
            mobile.val(response.data.mobile);
            photo.text(response.data.photo);
            cv.val(response.data.cv);
        });
    });
    $(document).on('submit', '#editForm', function (e) {
        e.preventDefault();
        var studentId = $('.studentInfoUpdate').data('studentid');
        var form = new FormData(this);
        form.append('studentid', studentId);
        $.ajax({
            url: "info/update",
            type: "POST",
            data: form,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.error) {
                    $.each(data.error, function (index, item) {
                        $('.showSucess>ul>li').remove();
                        $('.showSucess').css('display', 'none');
                        $('.showError>ul>li').remove();
                        $('.showError').css('display', 'block');
                        $('.showError>ul').append('<li>' + item + '</li>')
                    });
                }
                if (data.success) {
                    $('.showError>ul>li').remove();
                    $('.showError').css('display', 'none');
                    $('.studentInfoTable').remove();
                    $('.editInfoButton').remove();
                    let new_html = "<table class='table studentInfoTable'><tr><th style='width:50%'>نام و نام خانوادگی</th><td>" + data.studentInfo.name + ' ' + data.studentInfo.lastname + "</td></tr><tr><th>ایمیل (غیر قابل ویرایش)</th><td>" + data.studentInfo.email + "</td></tr><tr><th>شماره تماس</th><td>" + data.studentInfo.phone + "</td></tr><tr><th>شماره همراه</th><td>" + data.studentInfo.mobile + "</td></tr><tr><th>تصویر شما</th><td><img src='http://myproject.local/media/studentPhoto/" + data.studentInfo.photo + "' alt='تصویر شما' width='150px' height='150px'></td> </tr> <tr> <th>توضیحات و رزومه شما</th><td>" + data.studentInfo.cv + "</td></tr></table><div class='box-body editInfoButton'><button type='button' class='btn btn-app studentEdit' data-toggle='modal' data-target='#modal-default' data-stid='" + data.studentInfo.id + "'><i class='fa fa-edit'></i> ویرایش و تکمیل اطلاعات</button>";
                    $('.table-responsive').append(new_html);
                    $('.showSucess>ul>li').remove();
                    $('.showSucess').css('display', 'block');
                    $('.showSucess>ul').append('<li>اطلاعات شما با موفقیت ثبت گردید</li>');
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'اطلاعات شما با موفقیت ثبت/ویرایش شد',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    $('#modal-default').hide();
                    $('.modal-backdrop').remove();
                }
            }
        });
    });
    $(document).on('submit', '#requestForm', function (e) {
        e.preventDefault();
        let requestSubject = $('#requestTitle').val();
        let requestLink = $('#requestLink').val();
        let requestDesc = $('#requestDesc').val();
        var student_id = $('.requestSubmit').data('stid');
        var form = new FormData(this);
        form.append('student_id', student_id);
        $.ajax({
            url: "request",
            type: "POST",
            data: form,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data)
            }
        });
    });
});


$(document).ready(function () {
    $('#tarikh1').persianDatepicker({
        altField: '#tarikhAlt1',
        altFormat: 'X',
        format: 'D MMMM YYYY',
        observer: true,
        initialValue: false,

    });
    $('#tarikh2').persianDatepicker({
        altField: '#tarikhAlt2',
        altFormat: 'X',
        format: 'D MMMM YYYY',
        observer: true,
        initialValue: false,

    });

});

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.updateCount', function (event) {
        event.preventDefault();
        var _this = $(this);
        var _courseId = _this.data('id');
        var _countOfCourse = $('.product_counter_' + _courseId);
        var _countValue = parseInt(_countOfCourse.text());
        var _action = _this.attr('id') === 'plus' ? 'plus' : 'minus';
        var _subtotal= $('.product_subtotal_' + _courseId);
        var _totalElement=$('#total');
        axios.post('/basket/update', {
            course_id: _courseId,
            action: _action
        }).then(response => {
            if (response.data.success) {
                _countValue = _action === 'plus' ? _countValue += 1 : _countValue -= 1;
                _countOfCourse.text(_countValue);
                _subtotal.text(response.data.subTotal);
                _totalElement.text(response.data.total);
            }else{
                alert(response.data.message)
            }
        })
            .catch((error) => {
                alert(error);
            })
    });
});