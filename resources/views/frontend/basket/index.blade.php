@extends('layouts.frontend.frontend')
@section('content')


    <table class="table table-bordered">
        <tr>
            <th>ردیف</th>
            <th>محصول</th>
            <th>تعداد</th>
            <th>قیمت واحد</th>
            <th>قیمت کل</th>
            <th>عملیات</th>
        </tr>
        @foreach($basketItems as $product_id=>$info)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{$info['course_item']->title}}</td>
                <td>
                    <a id="plus" class="updateCount" data-id="{{$info['course_item']->id}}"><i class="fa fa-plus count" ></i></a>
                    <p data-x="asd" class="product_counter_{{$info['course_item']->id}}">{{$info['count']}}</p>
                    <a id="minus" class="updateCount" data-id="{{$info['course_item']->id}}"><i class="fa fa-minus count" ></i></a>
                </td>
                <td>{{$info['course_item']->price}}</td>
                <td class="product_subtotal_{{$info['course_item']->id}}">{{$info['course_item']->price*$info['count']}}</td>
                <td>عملیات</td>
            </tr>

        @endforeach
    </table>
    <p  class="bg-danger">
        <span>جمع کل:</span>
        <span id="total">{{ \App\Services\Basket\BasketService::total() }}</span>
    </p>

    <a href="{{route('basket.checkout')}}"><button type="button" class="btn btn-success">تکمیل سفارش</button></a>
@endsection