@extends('layouts.frontend.frontend')
@section('content')
    <div class="courses_banner">
        <div class="container">
            <h3>ورود</h3>
            <p class="description">
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و
                متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده
                شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
                الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و
                دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای
                اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </p>
            <div class="breadcrumb1">
                <ul>
                    <li class="icon6"><a href="index.html">صفحه اصلی</a></li>
                    <li class="current-page">ورود</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //banner -->
    <div class="courses_box1">
        <div class="container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <form class="login" method="post">
                {{csrf_field()}}
                <p class="lead">خوش آمدید</p>
                <div class="form-group">
                    <input autocomplete="off" type="password" name="password" class="required form-control"
                           placeholder="رمزجدید را وارد نمایید">
                </div> <div class="form-group">
                    <input autocomplete="off" type="password" name="password_repeat" class="required form-control"
                           placeholder="رمز جدید را دوباره تکرار نمایید">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-lg1 btn-block" value="تغییر رمرز عبور">
                </div>
            </form>
        </div>
    </div>
@endsection
