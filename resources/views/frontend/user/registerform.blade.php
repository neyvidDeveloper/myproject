@extends('layouts.frontend.frontend')
@section('content')
    <div class="courses_banner">
        <div class="container">
            <h3>ثبت نام</h3>
            <p class="description">
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و
                متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده
                شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
                الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و
                دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای
                اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </p>
            <div class="breadcrumb1">
                <ul>
                    <li class="icon6"><a href="index.html">صفحه اصلی</a></li>
                    <li class="current-page">ثبت نام</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //banner -->
    <div class="courses_box1">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="login" method="post">
                {{ csrf_field() }}
                <p class="lead">{{ $title }}</p>
                <div class="form-group">
                    <input type="text" autocomplete="off" class="required form-control" placeholder="نام" name="name"
                           value="">
                </div>
                <div class="form-group">
                    <input type="text" autocomplete="off" class="required form-control" placeholder="نام خانوداگی"
                           name="lastname" value="">
                </div>
                <div class="form-group">
                    <input type="password" class="required form-control" placeholder="کلمه عبور" name="password"
                           value="">
                </div>
                <div class="form-group">
                    <input type="email" autocomplete="off" class="required form-control" placeholder="ایمیل"
                           name="email" value="">
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-lg1 btn-block" value="ثبت نام">
                </div>
                <p>آیا حساب کاربری دارید؟ <a href="{{ route('loginForm') }}">ورود</a></p>
            </form>
        </div>
    </div>
@endsection
