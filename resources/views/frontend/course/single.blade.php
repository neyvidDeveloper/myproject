@extends('layouts.frontend.frontend')
@section('content')

    <div class="col-xs-8">
        <div class="thumbnail">
            <div class="alert alert-info" role="alert">{{$courses->title}}</div>
            <div class="well">

                <p>
                    طول دوره:
                    {{$courses->timeOfCourse}} ساعت
                </p>
                <p>
                    روز های برگزاری:
                    {{$courses->present()->showDaysOfPresent}}
                </p>
                <p>
                    <span class="label label-success">
                           تاریخ شروع دوره:
                        {{$courses->present()->show_jalali_date['startOfPresent_jalali']}}

                    </span>
                    &nbsp;
                    <span class="label label-warning">

                    تاریخ پایان دوره:
                        {{$courses->present()->show_jalali_date['endOfPresentt_jalali']}}
                    </span>

                </p>
                <p>
                    قیمت دوره:
                    {{$courses->present()->show_nice_price}}
                </p>

            </div>

        </div>
    </div>
    <div class="col-xs-4">
        <div class="thumbnail">
            <div class="alert alert-success" role="alert">درباره استاد برگزار کننده دوره</div>
            <p>
                نام و نام خانوادگی :
                {{$courses->teacher->present()->show_name_whit_lastname}}
            </p>

        </div>
    </div>



@endsection
