@extends('layouts.frontend.frontend')
@section('content')
    @if($courses && count($courses)>0)
        @foreach($courses as $course)
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img src="" alt="عکس دوره">
                    <div class="caption">
                        <h3>{{ $course->title }}</h3>
                        <p>{{ $course->description }}</p>
                        <p>{{ $course->present()->show_nice_price }}</p>
                        <p>
                            <a href="{{route('courses.single').'?id='.$course->id}}" class="btn btn-default"
                               role="button">دیدن جزییات</a>
                            <a href="{{route('basket.add',[$course->id])}}" class="btn btn-warning" role="button">اضافه
                                کردن به سبد خرید</a>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach

    @endif


@endsection
