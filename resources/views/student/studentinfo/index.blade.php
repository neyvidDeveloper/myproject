@extends('layouts.student.panel')


@section('content')







    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle"
                     src="{{asset('/media/studentPhoto/'.$user->photo)}}"
                     alt="User profile picture">

                <h3 class="profile-username text-center">{{$user->name}}</h3>

                <p class="text-muted text-center">مهندس نرم افزار</p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>دنبال شونده</b> <a class="pull-left">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>دنبال کننده</b> <a class="pull-left">543</a>
                    </li>
                    <li class="list-group-item">
                        <b>دوستان</b> <a class="pull-left">13,287</a>
                    </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>دنبال کردن</b></a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">درباره من</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> تحصیلات</strong>

                <p class="text-muted">
                    لیسانس نرم افزار کامپیوتر
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> موقعیت</strong>

                <p class="text-muted">ایران، تهران</p>

                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> توانایی ها</strong>

                <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">laravel</span>
                </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> یادداشت</strong>

                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">فعالیت ها</a></li>
                <li><a href="#myinfo" data-toggle="tab">مشخصات من</a></li>
                <li><a href="#timeline" data-toggle="tab">تایم لاین</a></li>
                <li><a href="#settings" data-toggle="tab">درخواست تدریس</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                        <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{asset('img/user1-128x128.jpg')}}"
                                 alt="user image">
                            <span class="username">
                          <a href="#">جانی آیو</a>
                          <a href="#" class="pull-left btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                            <span class="description">ارسال شده در ۱۲ اردیبهشت ۱۳۹۶</span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                        </p>
                        <ul class="list-inline">
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> اشتراک
                                    گذاری</a></li>
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> لایک</a>
                            </li>
                            <li class="pull-right">
                                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> نظر
                                    (5)</a></li>
                        </ul>

                        <input class="form-control input-sm" type="text" placeholder="نظر">
                    </div>
                    <!-- /.post -->

                    <!-- Post -->
                    <div class="post clearfix">
                        <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{asset('img/user7-128x128.jpg')}}"
                                 alt="User Image">
                            <span class="username">
                          <a href="#">سارا روس</a>
                          <a href="#" class="pull-left btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                            <span class="description">ارسال شده ۲ ساعت پیش</span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                        </p>

                        <form class="form-horizontal">
                            <div class="form-group margin-bottom-none">
                                <div class="col-sm-9">
                                    <input class="form-control input-sm" placeholder="پاسخ">
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">ارسال
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.post -->

                    <!-- Post -->
                    <div class="post">
                        <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="{{  asset('/img/user6-128x128.jpg') }}"
                                 alt="User Image">
                            <span class="username">
                          <a href="#">آدام جونز</a>
                          <a href="#" class="pull-left btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                            <span class="description">ارسال شده ۲ روز پیش</span>
                        </div>
                        <!-- /.user-block -->
                        <div class="row margin-bottom">
                            <div class="col-sm-6">
                                <img class="img-responsive" src="{{asset('img/photo1.png')}}" alt="Photo">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <img class="img-responsive" src="{{asset('img/photo2.png')}}" alt="Photo">
                                        <br>
                                        <img class="img-responsive" src="{{asset('img/photo3.jpg')}}" alt="Photo">
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6">
                                        <img class="img-responsive" src="{{asset('img/photo4.jpg')}}" alt="Photo">
                                        <br>
                                        <img class="img-responsive" src="{{asset('img/photo3.png')}}" alt="Photo">
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <ul class="list-inline">
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> اشتراک
                                    گذاری</a></li>
                            <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> لایک</a>
                            </li>
                            <li class="pull-right">
                                <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> نظر
                                    (5)</a></li>
                        </ul>

                        <input class="form-control input-sm" type="text" placeholder="نظر">
                    </div>
                    <!-- /.post -->
                </div>
                <div class="active tab-pane" id="myinfo">

                    <p class="lead">مشخصات شخصی شما</p>

                    <div class="table-responsive">
                        <table class="table studentInfoTable">
                            <tr>
                                <th style="width:50%">نام و نام خانوادگی</th>
                                <td>{{ $user->present()->show_name_whit_lastname }}</td>
                            </tr>
                            <tr>
                                <th>ایمیل (غیر قابل ویرایش)</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>شماره تماس</th>
                                <td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <th>شماره همراه</th>
                                <td>{{ $user->mobile }}</td>
                            </tr>
                            <tr>
                                <th>تصویر شما</th>
                                <td><img src="{{asset('/media/studentPhoto/'.$user->photo)}}" alt="" width="150px"
                                         height="150px"></td>
                            </tr>
                            <tr>
                                <th>توضیحات و رزومه شما</th>
                                <td>{{ $user->cv }}</td>
                            </tr>
                        </table>


                        <div class="box-body editInfoButton">
                            <button type="button" class="btn btn-app studentEdit" data-toggle="modal"
                                    data-target="#modal-default" data-stid="{{$user->id}}"><i class="fa fa-edit"></i>
                                ویرایش و تکمیل اطلاعات
                            </button>

                        </div>


                    </div>

                    <div class="modal fade" id="modal-default">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">ویرایش و تکمیل اطلاعات</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-danger showError" style="display: none">
                                        <ul>

                                        </ul>
                                    </div>
                                    <div class="alert alert-success showSucess" style="display: none">
                                        <ul>

                                        </ul>
                                    </div>
                                    <form role="form" id="editForm" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">نام</label>
                                                <input type="text" class="form-control" name="name" id="name"
                                                       placeholder="نام"></div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">نام خانوادگی</label>
                                                <input type="text" class="form-control" name="lastname" id="lastname"
                                                       placeholder="نام خانوادگی">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">ایمیل</label>
                                                <input disabled type="text" class="form-control" name="email" id="email"
                                                       placeholder="ایمیل">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">شماره تماس</label>
                                                <input type="text" class="form-control" name="phone" id="phone"
                                                       placeholder="021 12345678">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">شماره همراه</label>
                                                <input type="text" class="form-control" name="mobile" id="mobile"
                                                       placeholder="0912*******">
                                            </div>
                                            <div class="form-group">
                                                <label for="studentPhoto">ارسال فایل</label>
                                                <span id="photo"></span>
                                                <input type="file" id="studentPhoto" name="studentPhoto">
                                                <p class="help-block"> تصویر حتما می بایست یکی از فرمت های jpeg,jpg,png
                                                    باشد</p>
                                            </div>

                                            <div class="form-group">
                                                <label>درباره شما</label>
                                                <textarea name="cv" id="cv" class="form-control" rows="3"
                                                          placeholder="درباره شما"></textarea>
                                            </div>
                                        </div>
                                        {{--<input type="submit" value="Upload Photo" class="btnSubmit" />--}}
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                            خروج
                                        </button>
                                        <button type="submit" class="btn btn-primary studentInfoUpdate"
                                                data-studentid="{{$user->id}}">ذخیره
                                        </button>
                                    </form>
                                </div>

                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->


                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <ul class="timeline timeline-inverse">
                        <!-- timeline time label -->
                        <li class="time-label">
                  <span class="bg-red">
                    ۱۲ اردیبهشت ۱۳۹۴
                  </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                                <h3 class="timeline-header"><a href="#">تیم پشتیبانی</a> یک ایمیل برای شما ارسال کرد
                                </h3>

                                <div class="timeline-body">
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                                    برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی
                                    می باشد.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs">ادامه</a>
                                    <a class="btn btn-danger btn-xs">حذف</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-user bg-aqua"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 5 دقیقه پیش</span>

                                <h3 class="timeline-header no-border"><a href="#">سارا</a> درخواست دوستی شما را قبول کرد
                                </h3>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-comments bg-yellow"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 27 دقیقه پیش</span>

                                <h3 class="timeline-header"><a href="#">جواد</a> در پست شما نظر گذاشت</h3>

                                <div class="timeline-body">
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است.
                                </div>
                                <div class="timeline-footer">
                                    <a class="btn btn-warning btn-flat btn-xs">نمایش نظر</a>
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline time label -->
                        <li class="time-label">
                  <span class="bg-green">
                   ۱۲ خرداد ۱۳۹۴
                  </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <li>
                            <i class="fa fa-camera bg-purple"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> 2 روز پیش</span>

                                <h3 class="timeline-header"><a href="#">مینا</a> تصویر آپلود کرد</h3>

                                <div class="timeline-body">
                                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                                </div>
                            </div>
                        </li>
                        <!-- END timeline item -->
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="settings">
                    @if(count($user->request)==0)
                        <form class="form-horizontal" id='requestForm'>
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="requestTitle" class="col-sm-2 control-label">موضوع تدریس</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="requestTitle" name="requestTitle"
                                           placeholder="موضوع تدریس">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="requestLink" class="col-sm-2 control-label">لینک نمونه</label>

                                <div class="col-sm-10">
                                    <input type="url" class="form-control" id="requestLink" name='requestLink'
                                           placeholder="URL">

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="requestDesc" class="col-sm-2 control-label">توضیح</label>

                                <div class="col-sm-10">
                                <textarea class="form-control" id="requestDesc" name="requestDesc"
                                          placeholder="توضیحاتی مختصر درباره‌ی مطالبی که در این آموزش ارائه خواهید کرد"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger requestSubmit"
                                            data-stid="{{ $user->id }}">
                                        ثبت درخواست
                                    </button>
                                </div>
                            </div>
                        </form>
                    @else

                        <div class="alert alert-success showSucess">
                            <ul>
                                <li>شما قبلا درخواست خود را ثبت نموده اید لطفا منتظر تایید مدیر سایت باشید</li>
                            </ul>
                        </div>
                    @endif

                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>






@endsection