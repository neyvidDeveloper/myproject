@extends('layouts.student.panel')

@section('content')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $title }}</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ردیف</th>
                        <th>عنوان دوره</th>
                        <th>توضیحات دوره</th>
                        <th>روز های برگزاری</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>

                    </tr>
                    @foreach($allCourseOfTeacher as $course)

                        <tr>
                            <th>{{$loop->iteration}}</th>
                            <th>{{$course->title}}</th>
                            <th>{{$course->description}}</th>
                            <th>{{$course->present()->showDaysOfPresent}}</th>
                            <th>{!!$course->present()->show_status!!}</th>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

@endsection