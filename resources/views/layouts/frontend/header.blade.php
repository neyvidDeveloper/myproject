<!DOCTYPE HTML>
<html>
<head>
    <title>آموزشگاه</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Learn Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design"/>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <link href="{{asset('css/bootstrap-3.1.1.min.css')}}" rel='stylesheet' type='text/css'/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Custom Theme files -->
    <link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="css/jquery.countdown.css"/>

{{--<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>--}}
<!----font-Awesome----->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">


    <!----font-Awesome----->
    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
    </script>

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="https://unpkg.com/persian-datepicker@1.1.3/dist/css/persian-datepicker.css"/>

    <script src="https://unpkg.com/persian-date@1.0.5/dist/persian-date.js"></script>
    <script src="https://unpkg.com/persian-datepicker@1.1.3/dist/js/persian-datepicker.js"></script>

</head>

<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">تنظیمات ناوبری</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">آموزشگاه</a>
        </div>
        <!--/.navbar-header-->
        <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" style="height: 1px;">
            <ul class="nav navbar-nav">
                @guest
                    <li class="dropdown">
                        <a href="{{ route('loginForm') }}"><i class="fa fa-user"></i><span>ورود</span></a>
                    </li>
                @endguest

                @auth()
                    @role('agent')
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="fa fa-user"></i><span>{{ \Illuminate\Support\Facades\Auth::user()->name}}</span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('admin') }}">پروفایل من</a></li>
                            <li><a href="{{ route('logout') }}">خروج</a></li>
                        </ul>
                    </li>
                    @endrole
                    @hasanyrole('student|teacher')
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="fa fa-user"></i><span>{{ \Illuminate\Support\Facades\Auth::user()->name}}</span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('student') }}">پروفایل من</a></li>
                            <li><a href="{{ route('logout') }}">خروج</a></li>
                        </ul>
                    </li>
                    @endhasanyrole

                @endauth
                <li class="dropdown">
                    <a href="{{route('course.index')}}" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-list"></i><span>دوره ها</span></a>
                    <ul class="dropdown-menu">

                        <li><a href="{{route('course.index')}}">دوره های آموزشی</a></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{route('basket.show')}}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i><span>سبد خرید</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('basket.show')}}">مشاهده سبد خرید</a></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-globe"></i><span>انگلیسی</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><span><i class="flags us"></i><span>انگلیسی</span></span></a></li>
                        <li><a href="#"><span><i class="flags newzland"></i><span>نیوزلند</span></span></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-search"></i><span>جستجو</span></a>
                    <ul class="dropdown-menu search-form">
                        <form>
                            <input type="text" class="search-text" name="s" placeholder="Search...">
                            <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                        </form>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <!--/.navbar-collapse-->
</nav>