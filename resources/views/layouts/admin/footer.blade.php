<!-- /.content-wrapper -->
<footer class="main-footer text-left">
    <strong>Copyleft &copy; 2014-2017 <a href="https://adminlte.io">Almsaeed Studio</a> & <a
                href="http://hosseinizadeh.ir/adminlte">Alireza Hosseinizadeh</a></strong>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">فعالیت ها</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">تولد غلوم</h4>

                            <p>۲۴ مرداد</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">آپدیت پروفایل سجاد</h4>

                            <p>تلفن جدید (800)555-1234</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">نورا به خبرنامه پیوست</h4>

                            <p>nora@example.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">کرون جابز اجرا شد</h4>

                            <p>۵ ثانیه پیش</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">پیشرفت کارها</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            ساخت پوستر های تبلیغاتی
                            <span class="label label-danger pull-left">70%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            آپدیت رزومه
                            <span class="label label-success pull-left">95%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            آپدیت لاراول
                            <span class="label label-warning pull-left">50%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            بخش پشتیبانی سایت
                            <span class="label label-primary pull-left">68%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">وضعیت</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">تنظیمات عمومی</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        گزارش کنترلر پنل
                        <input type="checkbox" class="pull-left" checked>
                    </label>

                    <p>
                        ثبت تمامی فعالیت های مدیران
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        ایمیل مارکتینگ
                        <input type="checkbox" class="pull-left" checked>
                    </label>

                    <p>
                        اجازه به کاربران برای ارسال ایمیل
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        در دست تعمیرات
                        <input type="checkbox" class="pull-left" checked>
                    </label>

                    <p>
                        قرار دادن سایت در حالت در دست تعمیرات
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">تنظیمات گفتگوها</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        آنلاین بودن من را نشان نده
                        <input type="checkbox" class="pull-left" checked>
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        اعلان ها
                        <input type="checkbox" class="pull-left">
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        حذف تاریخته گفتگوهای من
                        <a href="javascript:void(0)" class="text-red pull-left"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- FastClick -->
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="/bower_components/Chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/demo.js')}}"></script>
<script src="{{asset('/js/persian-date-0.1.8.min.js')}}"></script>
<script src="{{asset('/js/persian-datepicker-0.4.5.min.js')}}"></script>

{{--<script src="{{asset('/js/persian-date-0.1.8.min.js')}}"></script>--}}
{{--<script src="{{asset('/js/persian-datepicker-0.4.5.min.js')}}"></script>--}}

<script src="{{asset('/js/app.js')}}"></script>
<link rel="stylesheet" href="https://unpkg.com/persian-datepicker@1.1.3/dist/css/persian-datepicker.css">
<script src="https://unpkg.com/persian-date@1.0.5/dist/persian-date.js"></script>
<script src="https://unpkg.com/persian-datepicker@1.1.3/dist/js/persian-datepicker.js"></script>
<script>
    $(document).ready(function () {

        var to, from;
        to = $(".range-to-example").persianDatepicker({
            inline: true,
            altField: '.range-to-example-alt',
            altFormat: 'X',
            initialValue: false,
            "navigator": {
                "text": {
                    "btnNextText": ">>",
                    "btnPrevText": "<<"
                }
            },
            onSelect: function (unix) {
                to.touched = true;
                if (from && from.options && from.options.maxDate != unix) {
                    var cachedValue = from.getState().selected.unixDate;
                    from.options = {maxDate: unix};
                    if (from.touched) {
                        from.setDate(cachedValue);
                    }
                }
            }
        });
        from = $(".range-from-example").persianDatepicker({
            inline: true,
            altField: '.range-from-example-alt',
            altFormat: 'X',
            initialValue: false,
            "navigator": {
                "text": {
                    "btnNextText": ">>",
                    "btnPrevText": "<<"
                }
            },
            onSelect: function (unix) {
                from.touched = true;
                if (to && to.options && to.options.minDate != unix) {
                    var cachedValue = to.getState().selected.unixDate;
                    to.options = {minDate: unix};
                    if (to.touched) {
                        to.setDate(cachedValue);
                    }
                }
            }
        });
    });



</script>

</body>
</html>
