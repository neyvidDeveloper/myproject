<!-- right side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-right info">
                <p>علیرضا حسینی زاده</p>
                <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منو</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>اطلاعات کلی</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('student.info') }}"><i class="fa fa-circle-o"></i>مشخصات من</a>
                    </li>
                </ul>

            </li>
            @role('teacher')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>دوره های من</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('teacher.courses') }}"><i class="fa fa-circle-o"></i>لیست دوره های من </a>
                    </li>
                    <li class="active"><a href="{{ route('teacher.courses.create') }}"><i class="fa fa-circle-o"></i>ایجاد دوره جدید </a>
                    </li>
                </ul>

            </li>
            @endrole
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
