@extends('layouts.admin.admin')
@section('content')


    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $title }}</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ردیف</th>
                        <th>نام و نام خانوادگی</th>
                        <th>ایمیل</th>
                        <th>موضوع تدریس</th>
                        <th>درباره تدریس</th>
                        <th>عملیات</th>
                    </tr>

                    @foreach($allRequest as $request)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$request->user->present()->show_name_whit_lastname}}</td>
                            <td>{{$request->user->email}}</td>
                            <td>{{$request->subject}}</td>
                            <td>{{$request->description}}</td>
                            @if($request->status==0)
                                <td>
                                    <a href="{{ route('admin.user.request.confirm').'?id='.$request->id }}">تایید
                                        درخواست</a>
                                </td>
                            @else
                                <td>

                                   {!!$request->present()->show_status !!}
                                </td>
                            @endif
                        </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection