@extends('layouts.admin.admin')
@section('content')
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title  }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان گروه</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="عنوان گروه"
                               name="title" value="{{$current_group->title}}">
                    </div>
                    {{--{{dd($allGroup)}}--}}
                    <div class="form-group">
                        <label>دسته گروه</label>
                        {{--{{dd($allGroup)}}--}}
                        <p>هر گروه میتواند دسته اصلی باشد و یا زیر گروه دسته دیگری باشد</p>
                        <select class="form-control" name="groupParent">
                            <option value="0">دسته اصلی(مادر)</option>
                            @if(count($allGroup)>0)
                                @foreach($allGroup as $group)
                                    @if($group->id==$current_group->parent_id)
                                        <option selected value="{{$group->id}}">{{$group->title}}</option>
                                    @else
                                        <option value="{{$group->id}}">{{$group->title}}</option>
                                    @endif

                                @endforeach
                            @endif

                        </select>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
            </form>
        </div>
    </div>
@endsection