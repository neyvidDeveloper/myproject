@extends('layouts.admin.admin')
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $title }}</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ردیف</th>
                        <th>عنوان گروه</th>
                        <th>دسته گروه</th>
                        <th>عملیات</th>
                    </tr>

                    @foreach($allGroup as $group)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$group->title}}</td>
                            <td>{{ isset($group->parent) ? $group->parent->title : 'مادر'}}</td>
                            <td>
                                <a href="{{route('admin.group.delete').'?id='.$group->id}}">حذف</a>
                                <a href="{{route('admin.group.edit').'?id='.$group->id}}">ویرایش</a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection