@extends('layouts.admin.admin')
@section('content')

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $title }}</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="جستجو">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ردیف</th>
                        <th>عنوان دوره</th>
                        <th>مدرس دوره</th>
                        <th>ظرفیت دوره</th>
                        <th>قیمت دوره</th>
                        <th>زمان دوره</th>
                        <th>تاریخ شروع دوره</th>
                        <th>تاریخ پایان دوره</th>
                        <th>روزهای برگزاری</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>

                    @foreach($courses as $course)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$course->title}}</td>
                            <td>{{$course->teacher->present()->show_name_whit_lastname}}</td>
                            <td>{{$course->present()->show_capacity}}</td>
                            <td>{{$course->present()->show_nice_price}}</td>
                            <td>{{$course->present()->show_alltime_of_course}}</td>
                            <td>{{$course->present()->show_jalali_date['startOfPresent_jalali']}}</td>
                            <td>{{$course->present()->show_jalali_date['endOfPresentt_jalali']}}</td>
                            <td>{{$course->present()->showDaysOfPresent}}</td>
                            <td>{!! $course->present()->show_status_with_link !!}</td>
                            <td>
                                <a href="{{route('admin.course.delete').'?id='.$course->id}}">حذف</a>
                                <a href="{{route('admin.course.edit').'?id='.$course->id}}">ویرایش</a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@endsection
