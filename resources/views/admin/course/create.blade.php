@extends('layouts.admin.admin')
@section('content')

    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title  }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="عنوان دوره"
                               name="title">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">ظرفیت دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ظرفیت دوره"
                               name="capacity">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="قیمت دوره به تومان"
                               name="price">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">طول دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1"
                               placeholder="مدت زمان حدودی کل دوره به ساعت"
                               name="timeOfCourse">
                    </div>


                    <div class="form-group">
                        <div class="col-xs-6">
                            <label>
                                تاریخ شروع دوره‌:
                            </label>
                            <input type="hidden" name="startOfPresent" class="datepicker-demo range-from-example-alt">
                            <div class="range-from-example"></div>

                        </div>
                        <div class="col-xs-6">
                            <label>
                                تاریخ پایان دوره:
                            </label>
                            <input type="hidden" name="endOfPresent" class="datepicker-demo range-to-example-alt">
                            <div class="range-to-example"></div>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <br>

                    <div class="form-group">

                            <label>
                                روزهای برگزاری دوره :
                            </label>

                        @foreach($days as $key=>$day)

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="days[]" value="{{$key}}">
                                    {{$day}}
                                </label>
                            </div>
                        @endforeach


                    </div>
                    <br>
                    <div class="form-group">

                        <label>
                            وضعیت دوره:
                        </label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="optionsRadios1" value="1">
                                فعال
                            </label>
                            <label>
                                <input type="radio" name="status" id="optionsRadios1" value="0" checked>
                                غیر فعال
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">توصیحات دوره</label>
                            <textarea name="description" class="form-control" rows="3"
                                      placeholder="توصیحات دوره"></textarea>
                        </div>

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
            </form>
        </div>
    </div>
@endsection


