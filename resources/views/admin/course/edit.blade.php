@extends('layouts.admin.admin')
@section('content')

    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title  }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="عنوان دوره"
                               name="title" value="{{$currentCourse->title}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">ظرفیت دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ظرفیت دوره"
                               name="capacity" value="{{$currentCourse->capacity}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="قیمت دوره به تومان"
                               name="price" value="{{$currentCourse->price}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">طول دوره</label>
                        <input type="text" class="form-control" id="exampleInputEmail1"
                               placeholder="مدت زمان حدودی کل دوره به ساعت"
                               name="timeOfCourse" value="{{$currentCourse->timeOfCourse}}">
                    </div>

                    <div class="form-group">
                        <label>تاریخ شروع دوره</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="tarikh1" class="form-control pull-right courseDate" value="{{$currentCourse->present()->show_jalali_date['startOfPresent_jalali']}}">
                            <input type="hidden" id="tarikhAlt1" class="form-control pull-right" name="startOfPresent" value="{{$currentCourse->present()->convert_to_timeStamp['startOfPresent']}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>تاریخ پایان دوره</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="tarikh2" class="form-control pull-right courseDate" value="{{$currentCourse->present()->show_jalali_date['endOfPresentt_jalali']}}">
                            <input type="hidden" id="tarikhAlt2" class="form-control pull-right" name="endOfPresent" value="{{$currentCourse->present()->convert_to_timeStamp['endOfPresent']}}">
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<br>--}}



                    <div class="form-group">

                        <label>
                            روزهای برگزاری دوره :
                        </label>

                        @foreach($days as $key=>$day)
                            @if(in_array($key,unserialize($currentCourse->daysOfPresent)))
                                <div class="checkbox">
                                    <label>
                                        <input checked type="checkbox" name="days[]" value="{{$key}}">
                                        {{$day}}
                                    </label>
                                </div>
                            @else
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="days[]" value="{{$key}}">
                                        {{$day}}
                                    </label>
                                </div>
                            @endif
                        @endforeach


                    </div>
                    <br>
                    <div class="form-group">
                        <label>
                            وضعیت دوره:
                        </label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="status" id="optionsRadios1" value="1">
                                فعال
                            </label>
                            <label>
                                <input type="radio" name="status" id="optionsRadios1" value="0" checked>
                                غیر فعال
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">توصیحات دوره</label>
                            <textarea name="description" class="form-control" rows="3"
                                      placeholder="توصیحات دوره"> {{$currentCourse->description}}</textarea>
                        </div>

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
            </form>
        </div>
    </div>
@endsection


