<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration {

	public function up()
	{
        Schema::create('courses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('group_id')->unsigned();
            $table->string('title', 150);
            $table->text('description')->nullable();
            $table->date('startOfPresent')->nullable();
            $table->date('endOfPresent')->nullable();
            $table->integer('timeOfCourse')->nullable();
            $table->datetime('daysOfPresent')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
        });

    }

	public function down()
	{
		Schema::dropIfExists('courses');
	}
}