<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('amount')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('payable')->nullable();
            $table->integer('paymentMethod')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::dropIfExists('orders');
	}
}